angular.module('arq.config', [])
  .constant('AUTH_EVENTS', {
    "notAuthenticated": "auth-not-authenticated",
    "authenticated": "auth-authenticated",
    "changePwd": "change-password"
  })
  .constant('HEADER_VALUES', {
    "accessToken": "X-Access-Token"
  })
  .constant('URLS', {
    "autenticacion": "https://localhost:44300"
  })
  .constant('CREDENTIALS', {
    "usuario": "usuario",
    "password": "pasw"
  });
