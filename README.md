Generación de paquete
===================
#### <i class="icon-file"></i> Preparación paquete
**Ejecutar las siguientes rutinas gulp:**
> - **clean-dist**: Limpia la carpeta que forma el paquete. `dist`
> - **move-2-dist**: Copia todo el contenido de la carpeta `www/arq/**/*` a la carpeta `dist`

**Modificar variables:**
Modificar el fichero de constantes  `dist/js/constants.js`  para adaptarlas al entorno de desplige. Las constantes a modificar son:
> - **URLS.autenticacion**: Conexión del servicio de autenticación
> - **PATH.package**: Ruta relativa del paquete en la aplicación destino

#### <i class="icon-file"></i> Agregar paquete aplicación destino
**Descargar el paquete**
Previamente tendremos que haber añadido en el archivo `bower.json` la dependia referene al paquete a descargar.

      "devDependencies": {
        "ionic": "driftyco/ionic-bower#1.1.0",
        "loginDirective": "https://bitbucket.org/dapuicon/loginDirective2.git#master"
      }

`bower install`

**Añadir referencias**
