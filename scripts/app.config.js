angular.module('app.config', [])
  .constant('NAVIGATION_CONFIG', {
    "LOGIN_PAGE": "login",
    "INDEX_PAGE": "app.browse",
    "WHITE_PAGES": ["page1", "page2"]
  })
  .constant('APP_CONFIG', {
    "emisor": "1",
    "version": "1"
  });
